import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import firebase from 'firebase'
// import {initializeFirebase} from './push-notification.js'
Vue.config.productionTip = false

 // Initialize Firebase
 
const firebaseConfig = {
  apiKey: "AIzaSyBpX_Zkbk4I7Tj5jVtruHYC9NLTkIsG0sw",
  authDomain: "capstone-web-firebase.firebaseapp.com",
  databaseURL: "https://capstone-web-firebase.firebaseio.com",
  projectId: "capstone-web-firebase",
  storageBucket: "capstone-web-firebase.appspot.com",
  messagingSenderId: "959966253264",
  appId: "1:959966253264:web:8f1077a9c348116bf04ed5",
  measurementId: "G-VRJFX2PYZ7"
};
firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();
messaging.requestPermission().then(function() {
   //getToken(messaging);
   return messaging.getToken();
}).then(function(token){
console.log(token);
})
.catch(function(err) {
console.log('Permission denied', err);
});


messaging.onMessage(function(payload){
console.log('onMessage: ',payload);
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

