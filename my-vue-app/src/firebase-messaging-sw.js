import firebase from 'firebase'
mportScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');

const firebaseConfig = {
  apiKey: "AIzaSyBpX_Zkbk4I7Tj5jVtruHYC9NLTkIsG0sw",
  authDomain: "capstone-web-firebase.firebaseapp.com",
  databaseURL: "https://capstone-web-firebase.firebaseio.com",
  projectId: "capstone-web-firebase",
  storageBucket: "capstone-web-firebase.appspot.com",
  messagingSenderId: "959966253264",
  appId: "1:959966253264:web:8f1077a9c348116bf04ed5",
  measurementId: "G-VRJFX2PYZ7"
};
firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});
