import firebase from 'firebase'

var firebaseConfig = {
  apiKey: "AIzaSyBpX_Zkbk4I7Tj5jVtruHYC9NLTkIsG0sw",
  authDomain: "capstone-web-firebase.firebaseapp.com",
  databaseURL: "https://capstone-web-firebase.firebaseio.com",
  projectId: "capstone-web-firebase",
  storageBucket: "capstone-web-firebase.appspot.com",
  messagingSenderId: "959966253264",
  appId: "1:959966253264:web:8f1077a9c348116bf04ed5",
  measurementId: "G-VRJFX2PYZ7"
};

// Initialize Firebase

export const initializeFirebase = () => {
  firebase.initializeApp(firebaseConfig);
  // firebase.analytics();
}

// navigator.serviceWorker
//   .register('/my-sw.js')
//   .then((registration) => {
//     firebase.messaging().useServiceWorker(registration);
//   });

export const askForPermissioToReceiveNotifications = async () => {
  try {
    const messaging = firebase.messaging();
    await messaging.requestPermission();
    const token = await messaging.getToken();
    console.log('user token:', token);

    return token;
  } catch (error) {
    console.error(error);
  }
}

